(defproject fblthp "0.1.0"
  :license "NONE"
  :url "http://example.com/fblthp"
  :run {main-function {:file [src "main.pl"],
                       :goal main/main
                       :args? true}
        test-1        {:file [test "test.pl"],
                       :goal run_test,
                       :args? false}}
  :swipl-ld {exe {:cc-options ["-O3" "-Wall" "-g"],
                  :output     "build/fblthp"
                  :goal       true
                  :c-files    [[src "start.c"]],
                  :pl-files   [[src "main.pl"]]}}
  :vendors [swipl sicstus]
  :dependencies [[gitlab.Shillah/foobar "23e4bb48"]])
