% -*- mode: prolog -*-
% This is your main file

:- module('main2 ab', [main/1]).

count([], 0).
count([_|Rest], Count) :- count(Rest, RestCount), Count is RestCount + 1.
    
main(Argv) :- count(Argv, X), (X=1 -> writeln("You entered 1 Argument.")
                              ;       writeln("You didnt enter 1 Argument.")
                              ).
test :- main([1, 2]), main([1]).
