% -*- mode: prolog -*-

:- asserta(user:file_search_path(library, "/home/sura/.prol/repositories/foobar/3619ef7c/src/")).
:- asserta(user:file_search_path(src, "/home/sura/Projekte/prolingen/fblthp/src/fblthp/")).
:- asserta(user:file_search_path(build, "/home/sura/Projekte/prolingen/fblthp/build/")).

:- use_module(src(main)).
start(Atom) :- term_to_atom(Expr, Atom), main(Expr).
