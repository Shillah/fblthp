% -*- mode: prolog -*-
% This is your main file

:- module(main, [main/1]).

:- use_module(library(foobar/foobar), [foobar_fun/2]).

main(Argv) :- writeln("You entered:"), writeln(Argv).

entry_point(Atom) :- term_to_atom(Expr, Atom), main(Expr).
