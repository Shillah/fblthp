:- module(other, [maplist/3]).

maplist(_, [], []).
maplist(F, [H|L], [X|R]) :- call(F, H, X), maplist(F, L, R).

main :- print(1), nl.
