% -*- mode: prolog -*-
% This is a test  file

:- use_module(src(main), [main/1]).

run_test :- writeln("Calling with 1 Argument"), main([1]),
            writeln("Calling with 2 Arguments."), main([1, 2]).

